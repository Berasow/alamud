from .effect import Effect2
from mud.events import EquipEvent

class EquipEffect(Effect2):
    EVENT = EquipEvent
