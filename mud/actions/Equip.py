from .action import Action2
from mud.events import PushEvent

class EquipAction(Action2):
    EVENT = EquipEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "equip"
